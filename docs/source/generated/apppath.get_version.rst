apppath.get\_version
====================

.. currentmodule:: apppath

.. autofunction:: get_version