apppath.system\_open\_path\_utilities
=====================================

.. automodule:: apppath.system_open_path_utilities

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      open_app_path
      system_open_path
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
      :toctree:
      :template: custom_autosummary/class.rst
   
      AppPathSubDirEnum
   
   

   
   
   



