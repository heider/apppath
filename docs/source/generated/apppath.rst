﻿apppath
=======

.. automodule:: apppath

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
      :toctree:
   
      dist_is_editable
      get_version
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom_autosummary/module.rst
   :recursive:

   apppath.app_path
   apppath.entry_points
   apppath.system_open_path_utilities
   apppath.utilities

