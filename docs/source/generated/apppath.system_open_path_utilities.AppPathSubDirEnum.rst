apppath.system\_open\_path\_utilities.AppPathSubDirEnum
=======================================================

.. currentmodule:: apppath.system_open_path_utilities

.. autoclass:: AppPathSubDirEnum
   :members:
   :show-inheritance:
   :inherited-members:

   
   .. automethod:: __init__

   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~AppPathSubDirEnum.data
      ~AppPathSubDirEnum.config
      ~AppPathSubDirEnum.cache
      ~AppPathSubDirEnum.log
   
   