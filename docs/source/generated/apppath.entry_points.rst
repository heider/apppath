apppath.entry\_points
=====================

.. automodule:: apppath.entry_points

   
   
   

   
   
   

   
   
   

   
   
   



.. rubric:: Modules

.. autosummary::
   :toctree:
   :template: custom_autosummary/module.rst
   :recursive:

   apppath.entry_points.clean_apppath
   apppath.entry_points.open_apppath

